nltk==3.8.1
python-dotenv==0.21.1
mysql-connector-python==8.0.32
discord==2.2.2
scikit-learn==1.2.2