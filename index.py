import discord
import os
from dotenv import load_dotenv
from discord.ext import commands
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from Menu import Menu
from MenuPreguntasNuevas import MenuPreguntasNuevas
from database import connect_database
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics.pairwise import cosine_similarity
from keep_alive import keep_alive
import mysql.connector


load_dotenv()

TOKEN = os.getenv('TOKEN_DISCORD')

mybd = None

nltk.download('stopwords')
nltk.download('punkt')

intents = discord.Intents.all()

bot = commands.Bot(command_prefix="!", intents=intents)  # Prefijo del bot


stop_words = set(stopwords.words('spanish'))


def process_question(question, db_data):
    # Tokenizamos la pregunta y eliminamos las stop words
    tokens = word_tokenize(question.lower())
    filtered_tokens = [token for token in tokens if token not in stop_words]

    # Preparamos los datos para el modelo de clasificación
    questions = [[token for token in word_tokenize(
        row[1].lower()) if token not in stop_words] for row in db_data]
    answers = [row[2] for row in db_data]

    vectorizer = CountVectorizer()
    vectorized_questions = vectorizer.fit_transform(
        [" ".join(q) for q in questions])

    # Creamos el modelo de clasificación
    classifier = MultinomialNB()
    classifier.fit(vectorized_questions, answers)

    # Clasificamos la pregunta del usuario y devolvemos la respuesta
    vectorized_question = vectorizer.transform([" ".join(filtered_tokens)])
    predicted_answer = classifier.predict(vectorized_question)[0]

    # Calculamos la similitud del coseno entre la pregunta y la respuesta
    vectorized_answers = vectorizer.transform(answers)
    similarities = cosine_similarity(
        vectorized_question, vectorized_answers)[0]

    # Comparamos la similitud con el umbral y devolvemos la respuesta si es mayor
    threshold = 0.2
    if max(similarities) > threshold:
        return [db_data[similarities.argmax()][0], predicted_answer]
    else:
        return [None, None]


@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))

@bot.command(name="pregunta")
async def faq(ctx, *, question):
    """
    Responde una pregunta frecuente utilizando NLTK para procesamiento de lenguaje natural.
    """
    try:
        # Crea una conexión a la base de datos
        mybd = connect_database()

        # Crea un cursor para ejecutar consultas
        cursor = mybd.cursor()

        # Obtiene las preguntas y respuestas almacenadas en la base de datos
        cursor.execute("""
            SELECT id,question, answer
            FROM support_chatbot
        """)
        db_data = cursor.fetchall()

        # Procesa la pregunta y obtiene la respuesta
        result = process_question(question, db_data)

        # Envía la respuesta al usuario
        if result[1]:
            await ctx.send(f'Hola {ctx.author.mention}, aquí está la respuesta a tu pregunta:')
            formatted_answers = '\n\n'.join(
                [f'{i}. {a}' for i, a in enumerate([result[1]], 1)])
            await ctx.send(f'```{formatted_answers}```')

            # Crea un menú para que el usuario pueda dar feedback sobre la respuesta
            view = Menu(db_data, result[0], mybd)
            await ctx.reply("Te sirvio la respuesta?", view=view)

            await ctx.send('¿Te puedo ayudar con algo más?')
        else:
            await ctx.send('Lo siento, no tengo una respuesta para esa pregunta por favor comuniquese con el admin del canal para tener una respuesta a la brevedad.')
            view = MenuPreguntasNuevas(question, mybd)
            await ctx.reply("Queres que analizemos esta pregunta para futuras preguntas?", view=view)

    except mysql.connector.Error as error:
        print(f"Error al conectarse a la base de datos: {error}")
        await ctx.send("Lo siento, no pude conectarme a la base de datos en este momento. Intente más tarde.")

@bot.command(name="deprecadas")
#@commands.has_permissions(administrator=True)
async def faq(ctx):
    try:
      mybd = connect_database()
      cursor = mybd.cursor()

      print("Establecida la conexión a la base de datos.")

      cursor.execute("""
        SELECT id, answer, helpful_responses, unhelpful_responses
        FROM support_chatbot
        WHERE state_answer = 'inappropriate'
      """)
      rows = cursor.fetchall()
      if not rows:
        await ctx.send("No se encontraron respuestas inapropiadas.")
        return

      formatted_answers = '\n\n'.join([f'{i}. \n -Respuesta: {row[1]} \n -ID: {row[0]} \n -Porcentaje de respuesta util: {row[2]/(row[2]+row[3]):.2%} \n - Porcentaje de respuesta no util: {row[3]/(row[2]+row[3]):.2%}' for i, row in enumerate(rows, 1)])
      message = f"Respuestas inapropiadas:\n{formatted_answers}"

      await ctx.send(f'```{message}```')
      cursor.close()

    except mysql.connector.Error as error:
      print(f"Error al conectarse a la base de datos: {error}")
      await ctx.send("Lo siento, no pude conectarme a la base de datos en este momento. Intente más tarde.")


@bot.command(name="analizar")
#@commands.has_permissions(administrator=True)
async def faq(ctx):
    try:
      mybd = connect_database()
      cursor = mybd.cursor()
      cursor.execute("""
          SELECT *
          FROM questions
      """)
      rows = cursor.fetchall()
      if not rows:
          await ctx.send("No se encontraron respuestas para analizar.")
          return
  
      formatted_answers = '\n\n'.join(
          [f'{i}. \n -ID: {row[0]} \n -Pregunta: {row[1]}' for i, row in enumerate(rows, 1)])
      message = f"Respuestas para analizar:\n{formatted_answers}"
  
      await ctx.send(f'```{message}```')
      cursor.close()
    except mysql.connector.Error as error:
      print(f"Error al conectarse a la base de datos: {error}")
      await ctx.send("Lo siento, no pude conectarme a la base de datos en este momento. Intente más tarde.")

@bot.command(name="admin")
async def mention_admin(ctx):

    guild = ctx.guild
    owner = guild.owner
    await ctx.send(f"Comuniquese con, {owner.mention}, para que pueda resolver tu problema.")
  

keep_alive()

try:
  bot.run(TOKEN)
except discord.errors.HTTPException:
  print("\n\n\nBLOQUEADO POR RATE LIMITS\n\n\n")
  os.system("kill 1")
  os.system("python restarter.py")